resource "kubernetes_manifest" "cdr_middleware" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "middlewares.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "Middleware"
        plural   = "middlewares"
        singular = "middleware"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "cdr_ingress_route" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "ingressroutes.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "IngressRoute"
        plural   = "ingressroutes"
        singular = "ingressroute"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "cdr_ingres_route_tcp" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "ingressroutetcps.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "IngressRouteTCP"
        plural   = "ingressroutetcps"
        singular = "ingressroutetcp"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "cdr_ingres_route_udp" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "ingressrouteudps.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "IngressRouteUDP"
        plural   = "ingressrouteudps"
        singular = "ingressrouteudp"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "cdr_tls_option" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "tlsoptions.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "TLSOption"
        plural   = "tlsoptions"
        singular = "tlsoption"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "cdr_tls_store" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "tlsstores.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "TLSStore"
        plural   = "tlsstores"
        singular = "tlsstore"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "cdr_traefik_service" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "traefikservices.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "TraefikService"
        plural   = "traefikservices"
        singular = "traefikservice"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}

resource "kubernetes_manifest" "cdr_servers_transport" {
  provider = kubernetes-alpha

  manifest = {
    apiVersion = "apiextensions.k8s.io/v1beta1"
    kind       = "CustomResourceDefinition"
    metadata = {
      name = "serverstransports.traefik.containo.us"
    }
    spec = {
      group = "traefik.containo.us"
      names = {
        kind     = "ServersTransport"
        plural   = "serverstransports"
        singular = "serverstransport"
      }
      scope   = "Namespaced"
      version = "v1alpha1"
    }
  }
}
