resource "kubernetes_service_account" "default" {
  metadata {
    name      = var.service_account
    namespace = kubernetes_namespace.default.metadata[0].name
  }
}

resource "kubernetes_cluster_role" "traefik" {
  metadata {
    name = kubernetes_service_account.default.metadata[0].name
  }

  rule {
    api_groups = [
      "",
    ]
    resources = [
      "services",
      "endpoints",
      "secrets",
    ]
    verbs = [
      "get",
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = [
      "extensions",
      "networking.k8s.io",
    ]
    resources = [
      "ingresses",
      "ingressclasses",
    ]
    verbs = [
      "get",
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = [
      "extensions",
    ]
    resources = [
      "ingresses/status",
    ]
    verbs = [
      "update",
    ]
  }

  rule {
    api_groups = [
      "traefik.containo.us",
    ]
    resources = [
      "middlewares",
      "ingressroutes",
      "traefikservices",
      "ingressroutetcps",
      "ingressrouteudps",
      "tlsoptions",
      "tlsstores",
      "serverstransports",
    ]
    verbs = [
      "get",
      "list",
      "watch",
    ]
  }
}

resource "kubernetes_cluster_role_binding" "traefik" {
  metadata {
    name = kubernetes_service_account.default.metadata[0].name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_service_account.default.metadata[0].name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.default.metadata[0].name
    namespace = kubernetes_namespace.default.metadata[0].name
  }
}
