variable "kubeconfig" {
  description = "Path to the kubeconfig file"
  type        = string
}

variable "kubecontext" {
  description = "Kubeconfig context to use"
  type        = string
  default     = "default"
}

variable "namespace" {
  description = "Namespace where the proxy should be provisioned"
  type        = string
  default     = "traefik"
}

variable "service_account" {
  description = "Service account to be created for the Traefik application"
  type        = string
  default     = "traefik"
}

variable "acme_user_email" {
  description = "Email of the ACME certificates provider user"
  type        = string
}

variable "acme_api_endpoint" {
  description = "API endpoint of ACME certificates provider"
  type        = string
  default     = "https://acme-staging-v02.api.letsencrypt.org/directory"
}

variable "acme_volume_storage_class_name" {
  description = "Cluster's storage class name for ACME volume"
  type        = string
  default     = "do-block-storage"
}

variable "acme_volume_storage_size" {
  description = "Size of the ACME volume"
  type        = string
  default     = "1Gi"
}

variable "log_level" {
  description = "Level of Traefik logging"
  type        = string
  default     = "WARN"
}

variable "prometheus_buckets" {
  description = "Buckets for Traefik's prometheus metrics"
  type        = list(number)

  default = [
    0.1,
    0.3,
    1.2,
    5.0
  ]
}

variable "traefik_image_version" {
  description = "Version of the Traefik container image to use"
  type        = string
  default     = "v2.4.2"
}

variable "dashboard_users_htpasswd" {
  description = "Content of the dashboard users htpasswd file"
  type        = string
}

variable "traefik_hostname" {
  description = "Hostname of Traefik's installation"
  type        = string
  default     = "tf"
}

variable "domain" {
  description = "Traefik's domain"
  type        = string
}
