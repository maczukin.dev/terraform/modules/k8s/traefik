# Traefik in Kubernetes terraform module

Terraform module to configure Traefik installation in Kubernetes.

## Usage

The volume depends on an experimental `kubernetes-alpha` provider for terraform.
There are also race conditions that can happen between creation of different
resources.

Therefore, at first usage, the resources should be applied.

For example, if the module is added to your Terraform configuration as `terraform`,
you should run these commands at first usage:

```shell
# apply required CDR definitions
terraform apply \
  -target "module.traefik.kubernetes_manifest.cdr_middleware" \
  -target "module.traefik.kubernetes_manifest.cdr_ingress_route" \
  -target "module.traefik.kubernetes_manifest.cdr_traefik_service"
# apply creation of the namespace
terraform apply \
  -target "module.traefik.kubernetes_namespace.traefik"
# apply the rest of the configuration
terraform apply
```

## Author

Tomasz Maczukin, 2021

## License

MIT
