terraform {
  required_version = "~> 0.14"

  required_providers {
    kubernetes-alpha = {
      version = "~> 0.2"
      source  = "hashicorp/kubernetes-alpha"
    }

    kubernetes = {
      version = "~> 2.0"
      source  = "hashicorp/kubernetes"
    }
  }
}
