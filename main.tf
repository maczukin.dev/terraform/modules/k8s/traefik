provider "kubernetes" {
  config_path    = var.kubeconfig
  config_context = var.kubecontext
}

provider "kubernetes-alpha" {
  server_side_planning = true

  config_path    = var.kubeconfig
  config_context = var.kubecontext
}

resource "kubernetes_namespace" "default" {
  provider = kubernetes

  metadata {
    name = var.namespace
  }
}

resource "kubernetes_config_map" "configuration" {
  metadata {
    name      = "traefik-configuration"
    namespace = kubernetes_namespace.default.metadata[0].name
  }

  data = {
    "traefik.toml" = <<EOS
[ping]

[api]
  dashboard = true

[log]
  level = "${var.log_level}"
  format = "json"

[accessLog]
  format = "json"

[metrics]
  [metrics.prometheus]
    buckets = [${join(", ", formatlist("%0.2f", var.prometheus_buckets))}]
    addEntryPointsLabels = true
    addServicesLabels = true
    entryPoint = "websecure"
    manualRouting = true

[providers]
  [providers.kubernetescrd]

[certificatesResolvers]
  [certificatesResolvers.acme]
    [certificatesResolvers.acme.acme]
      email = "${var.acme_user_email}"
      storage = "/etc/traefik/acme/acme.json"
      caServer = "${var.acme_api_endpoint}"

      [certificatesResolvers.acme.acme.httpChallenge]
        entryPoint = "web"

[entrypoints]
  [entrypoints.web]
    address = ":9080"
  [entrypoints.websecure]
    address = ":9443"
    [entrypoints.websecure.http]
      [entrypoints.websecure.http.tls]
        certresolver = "acme"
EOS
  }
}

resource "kubernetes_persistent_volume_claim" "traefik_acme" {
  metadata {
    name      = "traefik-acme-store"
    namespace = kubernetes_namespace.default.metadata[0].name
  }

  spec {
    access_modes = [
      "ReadWriteOnce"
    ]

    storage_class_name = var.acme_volume_storage_class_name

    resources {
      requests = {
        storage = var.acme_volume_storage_size
      }
    }
  }
}

resource "kubernetes_deployment" "default" {
  metadata {
    name      = "traefik"
    namespace = kubernetes_namespace.default.metadata[0].name
    labels = {
      app = "traefik"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "traefik"
      }
    }

    template {
      metadata {
        labels = {
          app = "traefik"
        }
      }
      spec {
        service_account_name = kubernetes_service_account.default.metadata[0].name

        container {
          name  = "traefik"
          image = "traefik:${var.traefik_image_version}"
          args = [
            "--configFile",
            "/etc/traefik/traefik.conf"
          ]

          readiness_probe {
            http_get {
              path = "/ping"
              port = 8080
            }
            failure_threshold     = 1
            initial_delay_seconds = 10
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 2
          }

          liveness_probe {
            http_get {
              path = "/ping"
              port = 8080
            }

            failure_threshold     = 3
            initial_delay_seconds = 10
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 2
          }

          port {
            name           = "web"
            container_port = 9080
          }

          port {
            name           = "websecure"
            container_port = 9443
          }

          volume_mount {
            mount_path = "/etc/traefik"
            name       = "config"
          }

          volume_mount {
            mount_path = "/etc/traefik/acme"
            name       = "acme"
          }
        }

        volume {
          name = "config"
          config_map {
            name = kubernetes_config_map.configuration.metadata[0].name
          }
        }

        volume {
          name = "acme"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim.traefik_acme.metadata[0].name
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "default" {
  metadata {
    name      = "traefik"
    namespace = kubernetes_namespace.default.metadata[0].name
  }

  spec {
    type = "LoadBalancer"
    selector = {
      app = "traefik"
    }

    port {
      protocol    = "TCP"
      port        = 80
      name        = "web"
      target_port = 9080
    }

    port {
      protocol    = "TCP"
      port        = 443
      name        = "websecure"
      target_port = 9443
    }
  }
}

resource "kubernetes_secret" "traefik_htpasswd" {
  metadata {
    name      = "traefik-htpasswd"
    namespace = kubernetes_namespace.default.metadata[0].name
  }

  data = {
    users = var.dashboard_users_htpasswd
  }
}

resource "kubernetes_manifest" "traefik_auth_middleware" {
  depends_on = [
    kubernetes_manifest.cdr_middleware
  ]

  provider = kubernetes-alpha

  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"

    metadata = {
      name      = "traefik-auth"
      namespace = kubernetes_namespace.default.metadata[0].name
    }

    spec = {
      basicAuth = {
        secret = kubernetes_secret.traefik_htpasswd.metadata[0].name
      }
    }
  }
}

resource "kubernetes_manifest" "traefik_http_to_https_redirect" {
  depends_on = [
    kubernetes_manifest.cdr_middleware
  ]

  provider = kubernetes-alpha

  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"

    metadata = {
      name      = "traefik-http-to-https-redirect"
      namespace = kubernetes_namespace.default.metadata[0].name
    }

    spec = {
      redirectScheme = {
        scheme    = "https"
        permanent = true
      }
    }
  }
}

resource "kubernetes_manifest" "dashboard_ingress_route" {
  depends_on = [
    kubernetes_manifest.cdr_middleware,
    kubernetes_manifest.cdr_ingress_route,
    kubernetes_manifest.cdr_traefik_service
  ]

  provider = kubernetes-alpha

  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"

    metadata = {
      name      = "traefik-dashboard"
      namespace = kubernetes_namespace.default.metadata[0].name
    }

    spec = {
      entryPoints = [
        "websecure",
      ]
      routes = [
        {
          match = "Host(`${var.traefik_hostname}.${var.domain}`)"
          kind  = "Rule"
          middlewares = [
            {
              name      = kubernetes_manifest.traefik_auth_middleware.object.metadata.name
              namespace = kubernetes_namespace.default.metadata[0].name
            }
          ]
          services = [
            {
              name = "api@internal"
              kind = "TraefikService"
            }
          ]
        },
        {
          match = "Host(`${var.traefik_hostname}.${var.domain}`)&&PathPrefix(`/metrics`)"
          kind  = "Rule"
          middlewares = [
            {
              name      = kubernetes_manifest.traefik_auth_middleware.object.metadata.name
              namespace = kubernetes_namespace.default.metadata[0].name
            }
          ]
          services = [
            {
              name = "prometheus@internal"
              kind = "TraefikService"
            }
          ]
        }
      ]
    }
  }
}

resource "kubernetes_manifest" "dashboard_redirects_ingress_route" {
  depends_on = [
    kubernetes_manifest.cdr_middleware,
    kubernetes_manifest.cdr_ingress_route,
    kubernetes_manifest.cdr_traefik_service
  ]

  provider = kubernetes-alpha

  manifest = {
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "IngressRoute"

    metadata = {
      name      = "traefik-dashboard-redirects"
      namespace = kubernetes_namespace.default.metadata[0].name
    }

    spec = {
      entryPoints = [
        "web",
      ]
      routes = [
        {
          match = "Host(`${var.traefik_hostname}.${var.domain}`)"
          kind  = "Rule"
          middlewares = [
            {
              name      = kubernetes_manifest.traefik_http_to_https_redirect.object.metadata.name
              namespace = kubernetes_namespace.default.metadata[0].name
            }
          ]
          services = [
            {
              name = "api@internal"
              kind = "TraefikService"
            }
          ]
        },
        {
          match = "Host(`${var.traefik_hostname}.${var.domain}`)&&PathPrefix(`/metrics`)"
          kind  = "Rule"
          middlewares = [
            {
              name      = kubernetes_manifest.traefik_http_to_https_redirect.object.metadata.name
              namespace = kubernetes_namespace.default.metadata[0].name
            }
          ]
          services = [
            {
              name = "prometheus@internal"
              kind = "TraefikService"
            }
          ]
        }
      ]
    }
  }
}
