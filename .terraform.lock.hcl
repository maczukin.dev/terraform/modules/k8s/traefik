# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.0.2"
  constraints = "~> 2.0"
  hashes = [
    "h1:PRfDnUFBD4ud7SgsMAa5S2Gd60FeriD1PWE6EifjXB0=",
    "zh:4e66d509c828b0a2e599a567ad470bf85ebada62788aead87a8fb621301dec55",
    "zh:55ca6466a82f60d2c9798d171edafacc9ea4991aa7aa32ed5d82d6831cf44542",
    "zh:65741e6910c8b1322d9aef5dda4d98d1e6409aebc5514b518f46019cd06e1b47",
    "zh:79456ca037c19983977285703f19f4b04f7eadcf8eb6af21f5ea615026271578",
    "zh:7c39ced4dc44181296721715005e390021770077012c206ab4c209fb704b34d0",
    "zh:86856c82a6444c19b3e3005e91408ac68eb010c9218c4c4119fc59300b107026",
    "zh:999865090c72fa9b85c45e76b20839da51714ae429d1ab14b7d8ce66c2655abf",
    "zh:a3ea0ae37c61b4bfe81f7a395fb7b5ba61564e7d716d7a191372c3c983271d13",
    "zh:d9061861822933ebb2765fa691aeed2930ee495bfb6f72a5bdd88f43ccd9e038",
    "zh:e04adbe0d5597d1fdd4f418be19c9df171f1d709009f63b8ce1239b71b4fa45a",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes-alpha" {
  version     = "0.2.1"
  constraints = "~> 0.2"
  hashes = [
    "h1:ZhzRD8WeAytKb8EcCwFvFq6m/OiG+BrGHFpfsirOp4g=",
    "zh:00f6f8c4fe7842406fd254a8d50f3d83e9051b74a556a0b117994b0eb4044333",
    "zh:28399ad53334cffa9db68d50c8d16e2043f657c940b7faa8e506811df896a8e6",
    "zh:755077d11fe89e0ccadd2d2674011b22680e6975f7b50afc740359a08dd44181",
    "zh:8000b66a452c147d6e5def3531d8e007a0ebc104ec4d2e60f110051b17e97a7f",
    "zh:803a1ef06efcbebedb47c88830491ee97be8016ac11001dde5914df38541ee87",
    "zh:96da73b6bc09af7d9cc4659e85587b3517251b846c3ba34c1b62e432fff28cd2",
    "zh:c593b15b967dd60eef5f2515f134b23b98045a3077684fe14b8a22487039d7bd",
    "zh:d150232daf813945e8749ad01b4bb873d3bdf2f79e64fe31f467fa3798c0615e",
    "zh:e10b7aded704cd2cc5ee28c535d5a868667d95acd2a156f8cbf398514213e759",
    "zh:f98a423d038552b5166f9a07a56d83331e6aceacdf0f7c7a6fa9bff414f4d91a",
  ]
}
