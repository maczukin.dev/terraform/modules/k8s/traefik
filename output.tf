output "traefik_ip" {
  value = kubernetes_service.default.status[0].load_balancer[0].ingress[0].ip
}

output "traefik_hostname" {
  value = var.traefik_hostname
}
